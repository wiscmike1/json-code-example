# README #
AJAX - JSON coding example

### What is this repository for? ###

* Based on date entered, accesses NASA's Curiosity rovery image data for that day	
* and displays sorted data in a table (defaults to current day when page first loads.
*	Table Columns
**		Camera Full Name
**		Camara Name (Acronym)
**		Image Link
* Version 1
* Uses JavaScript, HTML, CSS and some JQuery
* All code is my own other than JavaScript table sorting routine which I pulled from https://www.w3schools.com/howto/howto_js_sort_table.asp 
* CSS file is a public domain source gathered from a training session that I used as a base and added styles for the web page used
* Built using Brackets IDE.

### How do I get set up? ###

* Should be able to unzip project file and run Index.html 
